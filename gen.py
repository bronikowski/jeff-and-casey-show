import datetime
import PyRSS2Gen
import os
import datetime
import urllib

files = os.listdir('.')
files.sort()
items = []

for f in files:
    s = os.stat(f)
    print f, ' ', datetime.datetime.fromtimestamp(s[9])
    if f.find('.MP3') == -1:
        continue
    item = PyRSS2Gen.RSSItem(
        title = os.path.basename(f),
        link = 'http://fuse.pl/jeffandcasey/%s' % urllib.quote(f),
        description = '<a href="http://fuse.pl/jeffandcasey/%s">%s</a>' % (urllib.quote(f),f),
        guid = PyRSS2Gen.Guid('http://fuse.pl/jeffandcasey/%s' % urllib.quote(f)),
        pubDate = datetime.datetime.fromtimestamp(s[9]),
        enclosure = PyRSS2Gen.Enclosure('http://fuse.pl/jeffandcasey/%s' % urllib.quote(f),s[6],'audio/mpeg'),
    )
    items.append(item)

rss = PyRSS2Gen.RSS2(
    title = 'Jeff and Casey Show: audio version',
    link = 'http://fuse.pl/jeffandcasey/',
    image = PyRSS2Gen.Image('http://fuse.pl/jeffandcasey/podcast.png','Jeff and Casey Show',None,80,80,'Banana!'),
    description = 'Here comes the bananas!',
    lastBuildDate = datetime.datetime.now(),
    items = items
    )

rss.items.sort(lambda x,y: cmp(y.pubDate, x.pubDate))
rss.write_xml(open('index.rss','w'))
