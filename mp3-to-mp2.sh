#!/bin/bash

if [ -e /tmp/mp2-lock ]; 
then 
    exit 0 
fi
touch /tmp/mp2-lock
while [ $# -ne 0 ]
do
            newext=$(echo "$1"|sed  's/\.mp4//g')
            ffmpeg -n -i "$1" -b:a 192k "$newext".MP3
                shift
            done
rm /tmp/mp2-lock
