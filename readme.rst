Jeff and Casey Show, the audio edition
======================================

Jeff Roberts and Casey Muratori are the two of the most amazing people I know. Fact. Their podcast, Jeff and Casey show, changed my life (for better or worse) and since they moved to YouTube (boo!) I lost an ability to enjoy their rants. 

This is a stack of *extremely hackish* tools I used to redo their YouTube videos into a set of MP3 with an RSS feed. You can checkout the final product at:

http://fuse.pl/jeffandcasey/index.rss
